# vs-test

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### What I was focusing on during making the task

- The `images-block` component.

  I tried to implement lazy loading for images. But I wanted to find a **Vue-way**
  solution not just connect a ready-made JS library.

  Futhermore, I wanted to reduce the loading time before user sees anything
  even more and made a structure where blur versions of the images are being
  loaded first (the blur images size less twice then regular ones).

  Also, in documentation I found the way to load images in **webp** format,
  what reduces the final files sizes as well.

- **SCSS** styles structure

  All measures in my files are relative and depend on the `l-height` variable.
  Yeah, probably the name is not very good but in general this is very convenient.
  You can change all the gaps, indents and so on changing the only one variable.

- The **underscore** service wrapper

  I wrapped all the `underscore` functions (at least all what I use) to be sure
  if next time they change anything I'd need to fix it only in one place.

- The navigation links come from the headers of response

  Which is handy but could be even better 😃 Frontend don't know anything about
  total amount of the pages but Backend does and it could have inserted such an
  information into headers as well. Otherwise I don't see any big benefits of
  using this approach.

  In the assessment description I found this:

  > interface labels (such as Page , Next , Prev etc.) aren't selectable by user

  To be honest I didn't quite understand this phrase.

- I moved all interfaces labels to separate file `/constants/dictionary.js`

  This is just a JavaScript object for learning purposes. For instance in our
  company (but I believe this is common practice) all the labels come from BE
  depending on selected language.

- I found a couple of annoying typos in the morning 😫

  I've fixed them but I know this is against the rules
