export function responseData(data, headers) {
    return {data, headers, error: ""};
}

export function responseError(error) {
    return {data: {}, error};
}
