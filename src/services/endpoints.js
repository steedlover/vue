import constants from "../constants/api";

const endpoints = {
    IMAGES_LIST: constants.API_URL + constants.API_VER + constants.API_LIST,
    IMAGE_DETAIL: constants.API_URL + constants.API_ID
};

function queryStringParams(url, obj) {
    const keys = Object.keys(obj),
        query = keys.reduce((acc, key, i) => {
            return acc + (i > 0 ? "&" : "") + key + "=" + obj[key];
        }, "?");
    return url + query;
}

function queryStringSlash(url, ...params) {
    return params.reduce((acc, el) => acc + "/" + el, url);
}

export function imageDetailsUrl(id, paramsObj) {
    let url = queryStringSlash(endpoints.IMAGE_DETAIL, id, paramsObj.width, paramsObj.height);
    if (typeof paramsObj.webp === 'boolean' && paramsObj.webp === true) {
        url += constants.API_WEBP;
    }
    if (paramsObj.blur && typeof paramsObj.blur === 'number' && paramsObj.blur > 0) {
        url = queryStringParams(url, {blur: paramsObj.blur})
    }
    return url;
}

export function imagesListUrl(page, limit) {
    return queryStringParams(endpoints.IMAGES_LIST, {page, limit});
}

export default endpoints;
