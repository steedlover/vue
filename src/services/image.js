import axios from "axios";
import constants from "../constants/images";
import {responseData,responseError} from "./data";
import {imagesListUrl} from "../services/endpoints";

export async function getImagesList(page, limit) {
    if (!page || typeof page !== "number" || page <= 0) {
        page = constants.DEFAULT_PAGE;
    }
    if (!limit || typeof limit !== "number" || limit <= 0) {
        limit = constants.DEFAULT_LIMIT;
    }

    try {
        const response = await axios.get(imagesListUrl(page, limit)).then(response => response);
        return responseData(response.data, response.headers);
    } catch (e) {
        return responseError(e);
    }
}
