import {
  extend,
  extendOwn,
  size as objectSize,
  keys as objectKeys,
  indexBy as _indexBy,
  isEmpty as empty
} from "underscore";

export function objectMerge(...params) {
  return params.reduce((e, a) => extend(e, a));
}

export function objectMergeOwn(...params) {
  return params.reduce((e, a) => extendOwn(e, a));
}

export function size(obj) {
  return objectSize(obj);
}

export function keys(obj) {
  return objectKeys(obj);
}

export function indexBy(list, iteratee, context) {
  return _indexBy(list, iteratee, context);
}

export function isEmpty(obj) {
  return empty(obj);
}
