export default {
    DEFAULT_PAGE: 1,
    DEFAULT_LIMIT: 30,
    MIN_LIMIT: 10,
    MAX_LIMIT: 100,
    STEP_LIMIT: 10,
    THUMB_WIDTH: 512,
    THUMB_HEIGHT: 341,
    EXTERNAL_WIDTH:800,
    EXTERNAL_HEIGHT:600,
    THUMB_BLUR: 5,
}
