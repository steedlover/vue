export default {
    API_URL: "https://picsum.photos",
    API_VER: "/v2",
    API_ID: "/id",
    API_LIST: "/list",
    API_WEBP: ".webp"
};
