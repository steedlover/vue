export default {
    NAV_PAGE: 'Page:',
    NAV_LIMIT: 'Images:',
    IMAGES_LIST_EMPTY_ERROR: 'Sorry the result is empty',
    HEADER_FILTER_PLACEHOLDER: 'Input an author name here...',
    HEADER_LIMIT_PLACEHOLDER: 'Images in the list:',
    FOOTER_COPYRIGHT: '℗ Copyright',
    FOOTER_VIM_SPONSOR: 'Created using NVIM'
}
