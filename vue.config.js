module.exports = {
    css: {
        loaderOptions: {
            scss: {
                additionalData: `
                    @import "@/styles/_mixins.scss";
                    @import "@/styles/_vars.scss";
                    @import "@/styles/_helpers.scss";
                    @import "@/styles/_typo.scss";
                    @import "@/styles/_buttons.scss";
                    @import "@/styles/_reset.scss";
                `
            }
        }
    }
};
